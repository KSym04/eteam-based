<?php
/**
 * Widget Name: Eteam - Slide Parallax
 * Widget ID: eteam-slice-parallax
 * Author: Eteam.dk
 * Author URI: http://eteam.dk
 */

class Eteam_Slice_Parallax_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'eteam-slice-parallax',
			esc_html__( 'Eteam - Slide Parallax', 'kickass' ),
			array(
				'description' => esc_html__( 'Create custom images parallax.', 'kickass' )
			),
			array(),
			array(
				'list-images' => array(
					'type' => 'repeater',
			        'label' => __( 'List Image Boxes' , 'kickass' ),
			        'item_name'  => __( 'Image Box', 'kickass' ),
			        'fields' => array(
													'box_image' => array(
														'type' => 'media',
														'label' => __( 'Image File', 'kickass' ),
														'library' => 'image',
														'fallback' => true,
													),
													'box_transition_speed' => array(
														'type' 	=> 'text',
														'label' => esc_html__( 'Transition Speed', 'kickass' )
													),
													'box_position_bottom' => array(
														'type' 	=> 'text',
														'label' => esc_html__( 'Position - Bottom', 'kickass' ),
														'default' => 'auto'
													),
													'box_position_left' => array(
														'type' 	=> 'text',
														'label' => esc_html__( 'Position - Left', 'kickass' ),
														'default' => 'auto'
													),
												)
				)
			),
			KICKASS_WIDGET_FOLDER_URI
		);
	}

	function initialize() {
		$this->register_frontend_styles(
			array(
				array(
					'eteam-slice-parallax',
					KICKASS_WIDGET_FOLDER_URI . basename( dirname( __FILE__ ) ) . '/css/style.css',
					array(),
					KICKASS_VERSION
				),
			)
		);
	}
}
siteorigin_widget_register( 'eteam-slice-parallax', __FILE__, 'Eteam_Slice_Parallax_Widget' );
