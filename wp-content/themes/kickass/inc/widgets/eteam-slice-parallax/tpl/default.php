<div class="eteam-slide-parallax-container parallax-img-container">
	<?php if ( $instance['list-images'] ) : ?>
			<?php foreach( $instance['list-images'] as $x ) { ?>
				<?php
					// get image source and set background
					$src = siteorigin_widgets_get_attachment_image_src( $x['box_image'] );
				?>
				<img src="<?php echo $src[0]; ?>" class="parallax-move"
						data-ps-z-index="1"
						data-ps-speed="<?php echo $x['box_transition_speed']; ?>"
						data-ps-vertical-position="<?php echo $x['box_position_bottom']; ?>"
						data-ps-horizontal-position="<?php echo $x['box_position_left']; ?>" />
			<?php } ?>
	<?php endif; ?>
</div>
