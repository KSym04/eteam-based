<?php if( $instance['tabs'] ) : ?>
	<div class="eteam-advanced-tab-container">
		<ul class="eat-navigation">
			<?php $xx = 1; foreach( $instance['tabs'] as $x ) { ?>
				<?php
					$src_icon_x = siteorigin_widgets_get_attachment_image_src( $x['tab_icon'] );
					$src_icon_hover_x = siteorigin_widgets_get_attachment_image_src( $x['tab_icon_hover'] );
				?>
				<li class="<?php echo ( $xx == 1  ) ? 'current' : ''; ?>">
					<a href="#tab-content<?php echo $xx; ?>">
						<img src="<?php echo $src_icon_x[0]; ?>" class="static-image" />
						<img src="<?php echo $src_icon_hover_x[0]; ?>" class="hover-image" />
						<?php echo $x['tab_title']; ?>
					</a>
				</li>
			<?php $xx++; } ?>
		</ul>
		<div class="eat-content-wrapper">
			<?php $yy = 1; foreach( $instance['tabs'] as $y ) { ?>
				<?php $src_y = siteorigin_widgets_get_attachment_image_src( $y['tab_background'] ); ?>
				<div id="tab-content<?php echo $yy; ?>" class="eat-content <?php echo ( $yy == 1  ) ? 'current' : ''; ?>" style="background: transparent url('<?php echo $src_y[0]; ?>') no-repeat center center;">
					<div class="tab-content-inner">
						<?php echo $y['tab_content']; ?>
					</div>
				</div>
			<?php $yy++; } ?>
		</div>
	</div>
<?php endif; ?>
