<?php
/**
 * Widget Name: Eteam - Advanced Tab
 * Widget ID: eteam-advanced-tab
 * Author: Eteam.dk
 * Author URI: http://eteam.dk
 */

class Eteam_Advanced_Tab_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'eteam-advanced-tab',
			esc_html__( 'Eteam - Advanced Tab', 'kickass' ),
			array(
				'description' => esc_html__( 'Create advanced tab.', 'kickass' )
			),
			array(),
			array(
				'tabs' => array(
					'type' => 'repeater',
			        'label' => __( 'Tabs' , 'kickass' ),
			        'item_name'  => __( 'Single Tab', 'kickass' ),
			        'fields' => array(
													'tab_background' => array(
														'type' => 'media',
														'label' => __( 'Tab Content Background', 'kickass' ),
														'library' => 'image',
														'fallback' => true
													),
													'tab_icon' => array(
														'type' => 'media',
														'label' => __( 'Tab Icon', 'kickass' ),
														'library' => 'image',
														'fallback' => true
													),
													'tab_icon_hover' => array(
														'type' => 'media',
														'label' => __( 'Tab Icon Hover', 'kickass' ),
														'library' => 'image',
														'fallback' => true
													),
													'tab_title' => array(
														'type' 	=> 'text',
														'label' => esc_html__( 'Tab Title' )
													),
													'tab_content' => array(
														'type' 	=> 'tinymce',
														'label' => esc_html__( 'Tab Content' ),
														'row' => 20
													),
												)
				)
			),
			KICKASS_WIDGET_FOLDER_URI
		);
	}

	function initialize() {
		$this->register_frontend_styles(
			array(
				array(
					'eteam-advanced-tab',
					KICKASS_WIDGET_FOLDER_URI . basename( dirname( __FILE__ ) ) . '/css/style.css',
					array(),
					KICKASS_VERSION
				),
			)
		);
	}
}
siteorigin_widget_register( 'eteam-advanced-tab', __FILE__, 'Eteam_Advanced_Tab_Widget' );
