<?php if( $instance['tabs'] ) : ?>
	<div class="eteam-simple-vertical-tab-container">
		<ul class="esvt-navigation">
			<?php $xx = 1; foreach( $instance['tabs'] as $x ) { ?>
				<?php
					$src_icon_x = siteorigin_widgets_get_attachment_image_src( $x['tab_icon'], 'full' );
				?>
				<li class="<?php echo ( $xx == 1  ) ? 'current' : ''; ?>">
					<a href="#vtab-content<?php echo $xx; ?>">
						<div class="media-icon">
							<img src="<?php echo $src_icon_x[0]; ?>" class="static-image" />
						</div>
						<div class="media-content">
							<h3><?php echo $x['tab_title']; ?></h3>
							<p><?php echo $x['tab_excerpt']; ?><p>
						</div>
					</a>
				</li>
			<?php $xx++; } ?>
		</ul>
		<div class="esvt-content-wrapper">
			<?php $yy = 1; foreach( $instance['tabs'] as $y ) { ?>
				<div id="vtab-content<?php echo $yy; ?>" class="esvt-content <?php echo ( $yy == 1  ) ? 'current' : ''; ?>">
					<div class="vtab-content-inner">
						<?php echo $y['tab_content']; ?>
					</div>
				</div>
			<?php $yy++; } ?>
		</div>
	</div>
<?php endif; ?>