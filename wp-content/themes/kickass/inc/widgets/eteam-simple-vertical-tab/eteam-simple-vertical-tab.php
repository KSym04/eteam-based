<?php
/**
 * Widget Name: Eteam - Simple Vertical Tab
 * Widget ID: eteam-simple-vertical-tab
 * Author: Eteam.dk
 * Author URI: http://eteam.dk
 */

class Eteam_Simple_Vertical_Tab_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'eteam-simple-vertical-tab',
			esc_html__( 'Eteam - Simple Vertical Tab', 'kickass' ),
			array(
				'description' => esc_html__( 'Create simple vertical tab.', 'kickass' )
			),
			array(),
			array(
				'tabs' => array(
					'type' => 'repeater',
			        'label' => __( 'Tabs' , 'kickass' ),
			        'item_name'  => __( 'Single Tab', 'kickass' ),
			        'fields' => array(
													'tab_icon' => array(
														'type' => 'media',
														'label' => __( 'Tab Icon', 'kickass' ),
														'library' => 'image',
														'fallback' => true
													),
													'tab_title' => array(
														'type' 	=> 'text',
														'label' => esc_html__( 'Tab Title' )
													),
													'tab_excerpt' => array(
														'type' 	=> 'text',
														'label' => esc_html__( 'Tab Excerpt' )
													),
													'tab_content' => array(
														'type' 	=> 'tinymce',
														'label' => esc_html__( 'Tab Content' ),
														'row' => 20
													)
												)
				)
			),
			KICKASS_WIDGET_FOLDER_URI
		);
	}

	function initialize() {
		$this->register_frontend_styles(
			array(
				array(
					'eteam-simple-vertical-tab',
					KICKASS_WIDGET_FOLDER_URI . basename( dirname( __FILE__ ) ) . '/css/style.css',
					array(),
					KICKASS_VERSION
				),
			)
		);
	}
}
siteorigin_widget_register( 'eteam-simple-vertical-tab', __FILE__, 'Eteam_Simple_Vertical_Tab_Widget' );