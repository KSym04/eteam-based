<?php
/**
 * Widget Name: Eteam - Advanced Tooltip
 * Widget ID: eteam-advanced-tooltip
 * Author: Eteam.dk
 * Author URI: http://eteam.dk
 */

class Eteam_Advanced_Tooltip_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'eteam-advanced-tooltip',
			esc_html__( 'Eteam - Advanced Tooltip', 'kickass' ),
			array(
				'description' => esc_html__( 'Create advanced accordion.', 'kickass' )
			),
			array(),
			array(
				'tooltips' => array(
					'type' => 'repeater',
			        'label' => __( 'Tooltips' , 'kickass' ),
			        'item_name'  => __( 'Tooltip Box', 'kickass' ),
			        'fields' => array(
													'tooltip_background' => array(
														'type' => 'media',
														'label' => __( 'Tooltip Background', 'kickass' ),
														'library' => 'image',
														'fallback' => true
													),
													'tooltip_title' => array(
														'type' 	=> 'text',
														'label' => esc_html__( 'Tooltip Title' )
													),
													'tooltip_content' => array(
														'type' 	=> 'tinymce',
														'label' => esc_html__( 'Tooltip Content' ),
														'row' => 20
													),
												)
				)
			),
			KICKASS_WIDGET_FOLDER_URI
		);
	}

	function initialize() {
		$this->register_frontend_styles(
			array(
				array(
					'eteam-advanced-tooltip',
					KICKASS_WIDGET_FOLDER_URI . basename( dirname( __FILE__ ) ) . '/css/style.css',
					array(),
					KICKASS_VERSION
				),
			)
		);
	}
}
siteorigin_widget_register( 'eteam-advanced-tooltip', __FILE__, 'Eteam_Advanced_Tooltip_Widget' );
