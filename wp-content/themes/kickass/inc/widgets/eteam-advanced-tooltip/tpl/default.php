<?php if( $instance['tooltips'] ) : ?>
<ul id="eteam-tooltips" class="eteam-tooltips">
	<?php foreach( $instance['tooltips'] as $t ) : ?>
		<?php $src = siteorigin_widgets_get_attachment_image_src( $t['tooltip_background'] ); ?>
		<li>
			<span class="tooltip-title"><?php echo $t['tooltip_title']; ?></span>
			<span class="tooltip-arrow"></span>
			<img src="<?php echo $src[0]; ?>" />
			<div class="content">
				<?php echo $t['tooltip_content']; ?>
			</div>
		</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>