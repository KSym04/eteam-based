<div class="eteam-list-icons">
	<?php if ( $instance['title'] ) : ?>
		<span class="list-icons-title eteam-list-links_tinymce-ext-plugins"><?php echo $instance['title']; ?></span>
	<?php endif; ?>
	<?php if ( $instance['list-icons'] ) : ?>
		<ul>
			<?php foreach( $instance['list-icons'] as $x ) { ?>
				<li>
					<?php if ( ! empty( $x['link'] ) ) echo '<a href="' . sow_esc_url( $x['link'] ) . '"' . ( $x['open_new_tab'] ? 'target="_blank"' : '' ) . ' alt="' . $x['label'] . '">'; ?>
							<div class="list-icon-media"><?php echo wp_get_attachment_image( $x['image'], 'full', true ); ?></div>
							<div class="list-icon-text"><?php echo $x['label']; ?></div>
					<?php if ( ! empty( $x['link'] ) ) echo '</a>'; ?>
				</li>
			<?php } ?>
		</ul>
	<?php endif; ?>
</div>
