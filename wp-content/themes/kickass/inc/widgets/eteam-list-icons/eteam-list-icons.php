<?php
/**
 * Widget Name: Eteam - List Icons
 * Widget ID: eteam-list-icons
 * Author: Eteam.dk
 * Author URI: http://eteam.dk
 */

class Eteam_List_Icons_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'eteam-list-icons',
			esc_html__( 'Eteam - List Icons', 'kickass' ),
			array(
				'description' => esc_html__( 'Display list of links.', 'kickass' )
			),
			array(),
			array(
				'title' => array(
					'type' 	=> 'text',
					'label' => esc_html__( 'Title', 'kickass' ),
				),
				'list-icons' => array(
					'type' => 'repeater',
			        'label' => __( 'List Icons' , 'kickass' ),
			        'item_name'  => __( 'Item', 'kickass' ),
			        'fields' => array(
													'label' => array(
														'type' 	=> 'text',
														'label' => esc_html__( 'Label', 'kickass' ),
													),
													'image' => array(
														'type' 	=> 'media',
														'label' => esc_html__( 'Icon', 'kickass' ),
													),
													'link' => array(
														'type' 	=> 'link',
														'label' => esc_html__( 'Link', 'kickass' ),
													),
													'open_new_tab' => array(
												        'type' => 'checkbox',
												        'label' => esc_html__( 'Do you want to open url in new tab?', 'kickass' ),
												        'default' => true
												  ),
												)
				)
			),
			KICKASS_WIDGET_FOLDER_URI
		);
	}

	function initialize() {
		$this->register_frontend_styles(
			array(
				array(
					'eteam-list-icons',
					KICKASS_WIDGET_FOLDER_URI . basename( dirname( __FILE__ ) ) . '/css/style.css',
					array(),
					KICKASS_VERSION
				),
			)
		);
	}
}
siteorigin_widget_register( 'eteam-list-icons', __FILE__, 'Eteam_List_Icons_Widget' );
