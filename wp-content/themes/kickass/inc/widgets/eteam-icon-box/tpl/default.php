<div class="kickass-icon-box">
	<?php
	$src = siteorigin_widgets_get_attachment_image_src(
		$instance['image'],
		$size,
		$image_fallback
	);

	$attr = array();
	if( !empty($src) ) {
		$attr = array(
			'src' => $src[0],
		);

		if(!empty($src[1])) $attr['width'] = $src[1];
		if(!empty($src[2])) $attr['height'] = $src[2];
		if (function_exists('wp_get_attachment_image_srcset')) {
			$attr['srcset'] = wp_get_attachment_image_srcset( $image, $size );
	 	}
	}
	$attr = apply_filters( 'siteorigin_widgets_image_attr', $attr, $instance, $this );

	$classes = array('');

	if( !empty( $title ) ) $attr['title'] = $title;
	if( !empty( $alt ) ) $attr['alt'] = $alt;

	?>
	<div class="kickass-icon-box-media">
	<?php if(!empty($url)) : ?><a href="<?php echo sow_esc_url($url) ?>" <?php if($new_window) echo 'target="_blank"' ?>><?php endif; ?>
		<img <?php foreach($attr as $n => $v) echo $n.'="' . esc_attr($v) . '" ' ?> class="<?php echo esc_attr( implode(' ', $classes) ) ?>"/>
	<?php if(!empty($url)) : ?></a><?php endif; ?>
	</div>
	<div class="kickass-icon-box-content">
		<span class="title"><?php echo wp_kses_post( $instance['title'] ); ?></span>

		<?php if(!empty($instance['make_it_link'])) : ?>
				<?php if( is_email( $instance['subtitle'] ) ) { $sublink =  'mailto:' . $instance['subtitle']; } ?>
				<?php
					if( strpos( $instance['subtitle'], '+' ) !== FALSE ) {
						$sublink =  'tel:+' . preg_replace("/[^0-9]/", "",$instance['subtitle']);
					}
				?>
			<a href="<?php echo $sublink; ?>" class="subtitle"><?php echo wp_kses_post( $instance['subtitle'] ); ?></a>
		<?php else : ?>
			<span class="subtitle"><?php echo wp_kses_post( $instance['subtitle'] ); ?></span>
		<?php endif; ?>
	</div>
</div>