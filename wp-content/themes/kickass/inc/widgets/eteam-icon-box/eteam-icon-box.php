<?php
/**
 * Widget Name: Eteam - Icon Box
 * Widget ID: eteam-icon-box
 * Author: Eteam.dk
 * Author URI: http://www.eteam.dk/
 */

class Eteam_Icon_Box_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'eteam-icon-box',
			esc_html__( 'Eteam - Icon Box', 'kickass' ),
			array(
				'description' => esc_html__( 'Box of for image, title and small text.', 'kickass' )
			),
			array(),
			array(
				'title' => array(
					'type' 	=> 'text',
					'label' => esc_html__( 'Title', 'kickass' ),
				),
				'subtitle' => array(
					'type' 	=> 'text',
					'label' => esc_html__( 'Content', 'kickass' ),
				),
				'make_it_link' => array(
					'type' => 'checkbox',
					'default' => false,
					'label' => __('You want to make subtitle as link?', 'kickass'),
				),
				'image' => array(
					'type' => 'media',
					'label' => __( 'Image file', 'kickass' ),
					'library' => 'image',
					'fallback' => true,
				),
			),
			KICKASS_WIDGET_FOLDER_URI
		);
	}

	function initialize() {
		$this->register_frontend_styles(
			array(
				array(
					'eteam-icon-box',
					KICKASS_WIDGET_FOLDER_URI . basename( dirname( __FILE__ ) ) . '/css/style.css',
					array(),
					KICKASS_VERSION
				),
			)
		);
	}
}
siteorigin_widget_register( 'eteam-icon-box', __FILE__, 'Eteam_Icon_Box_Widget' );
