<div class="eteam-list-links">
	<?php if ( $instance['title'] ) : ?>
		<h3><strong><?php echo $instance['title']; ?></strong></h3>
	<?php endif; ?>
	<?php if ( $instance['list-links'] ) : ?>
		<ul>
			<?php foreach( $instance['list-links'] as $x ) { ?>
				<li>
					<?php if ( ! empty( $x['link'] ) ) echo '<a href="' . sow_esc_url( $x['link'] ) . '"' . ( $x['open_new_tab'] ? 'target="_blank"' : '' ) . '>'; ?>
							<span class="bullet-arrow"></span>
							<?php echo $x['label']; ?>
					<?php if ( ! empty( $x['link'] ) ) echo '</a>'; ?>
				</li>
			<?php } ?>
		</ul>
	<?php endif; ?>
</div>
