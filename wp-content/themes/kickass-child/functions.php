<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'kickass_child_config_parent_css' ) ):
    function kickass_child_config_parent_css() {
        wp_enqueue_style( 'kickass_child_config_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'kickass_child_config_parent_css', 10 );

// END ENQUEUE PARENT ACTION

if ( ! function_exists( 'kickass_child' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function kickass_child() {
	// Register editor style
	add_editor_style( 'style.css' );
}
endif;
add_action( 'after_setup_theme', 'kickass_child' );

/**
 * Hook revolution slider
 */
function kickass_child_homepage_slider() {
    echo '<div class="kickass-child-homepage-slider-wrap">' 
            . do_shortcode( '[rev_slider alias="homepage_slider"]' ) . 
         '</div>';
}
add_action( 'kickass_child_after_header', 'kickass_child_homepage_slider' );

/**
 * Register custom JS
 */
function kickass_child_register_js() {
	wp_enqueue_script( 'kickass-child-custom-script', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'kickass_child_register_js', 10 );

/**
 * Functions to add SiteOrigin Widget framework.
 *
 * @package Kickass
 */

// Define base widget folder url.
define( 'KICKASS_CHILD_WIDGET_FOLDER_URI', get_stylesheet_directory_uri() . '/inc/widgets/' );

/**
 * Add new widgets list.
 *
 * @return array
 */
function kickass_child_widgets_collection( $folders ) {

	// Get widgets folder.
	$folders[] = get_stylesheet_directory() . '/inc/widgets/';

	// Return folders list.
	return $folders;
}
add_filter( 'siteorigin_widgets_widget_folders', 'kickass_child_widgets_collection' );